package com.hans88.absenadmin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.hans88.absenadmin.adapters.DataAdapter;
import com.hans88.absenadmin.adapters.SpinAdapter;
import com.hans88.absenadmin.models.mData.GDatas;
import com.hans88.absenadmin.models.mSpinner.GSpinner;

public class SpinnerActivity extends AppCompatActivity {

    Spinner spBar;
    String TAG;
    Context c;
    BE.PD pd;
    private GSpinner lists;
    private GDatas datalist;

    private RecyclerView rv;
    DataAdapter datas;

    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        c = this;
        TAG = c.getClass().getSimpleName();
        spBar = (Spinner) findViewById(R.id.spBar);
        rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(c));
        datas = new DataAdapter(c, datalist);
        rv.setAdapter(datas);
        btnAdd = findViewById(R.id.btnAdd);


        pd = new BE.PD(c);
        pd.show();
        AndroidNetworking.get("http://119.235.208.235:8085/api/admin-branch")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GSpinner.class, new ParsedRequestListener<GSpinner>() {
                    @Override
                    public void onResponse(GSpinner r) {
                        pd.dismiss();
                        lists = r;
                        if(lists.getData().size()>0) {
//                        List<String> listSpinner = new ArrayList<String>();
//                        for (int i = 0; i < r.getData().size(); i++){
//                            listSpinner.add(r.getData().get(i).getDisplay());
//                        }
//                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(c,
//                                android.R.layout.simple_spinner_item, listSpinner);
//                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                        spBar.setAdapter(adapter);

                            SpinAdapter adapter = new SpinAdapter(c, lists);
                            spBar.setAdapter(adapter);
                            spBar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//                                    Toast.makeText(c, lists.getData().get(pos).getValue(), Toast.LENGTH_SHORT).show();
                                    final String v = lists.getData().get(pos).getValue();
                                    initData(v);
                                    btnAdd.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Bundle b = new Bundle();
                                            Intent i = new Intent(c, MainActivity.class);
                                            b.putString("v",v);
                                            i.putExtras(b);
                                            startActivity(i);
                                            finish();
                                        }
                                    });
                                }

                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                        }else {
                            BE.TShort("Sorry, No Data Available");
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        pd.dismiss();
                        BE.TShort(error.getErrorDetail());
                        Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                        Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                        Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                    }
                });

    }

    private void initData(String v) {

        pd.show();
        AndroidNetworking.get("http://119.235.208.235:8085/api/branch-latlong?brchcode="+v)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GDatas.class, new ParsedRequestListener<GDatas>() {
                    @Override
                    public void onResponse(GDatas r) {
                        pd.dismiss();
                        if(r.getData().size()>0) {
                            datalist = r;
                            datas = new DataAdapter(c,datalist);
                            rv.setAdapter(datas);
                            datas.notifyDataSetChanged();
                        }else {
                            BE.TShort("Sorry, No Data Available");
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        pd.dismiss();
                        BE.TShort(error.getErrorDetail());
                        Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                        Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                        Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                    }
                });

    }
}
