package com.hans88.absenadmin.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hans88.absenadmin.R;
import com.hans88.absenadmin.models.mData.GDatas;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private GDatas lists;
    Context c;
    
    public DataAdapter(Context c, GDatas lists) {
        this.c = c;
        this.lists = lists;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_datas, parent, false);
        ViewHolder holder = new ViewHolder(v); //inisialisasi ViewHolder
        return holder;
    } //fungsi yang dijalankan saat ViewHolder dibuat

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        DataItem datas = lists.get(position).getData().getData().get(position);
        holder.mTitle.setText(lists.getData().get(position).getDisplay());
        holder.mPost.setText("Position: "+lists.getData().get(position).getPosition());
        holder.mLat.setText("Latitude: "+lists.getData().get(position).getLatitude());
        holder.mLong.setText("Longitude: "+lists.getData().get(position).getLongitude());

//        RequestOptions requestOptions = new RequestOptions();
//        requestOptions.placeholder(R.drawable.bg_loading);
//        requestOptions.error(R.drawable.bg_loading);
//        Glide.with(c)
//                .load(lists.getData().getData().get(position).getFeatureImage())
//                .apply(RequestOptions.fitCenterTransform()
//                        .placeholder(R.drawable.bg_loading)
//                        .error(R.drawable.bg_loading))
//                .apply(requestOptions)
//                .into(holder.mImage);
    }

    @Override
    public int getItemCount() {
        int a;
        if (lists != null && !lists.getData().isEmpty()) {
            a = lists.getData().size();
        } else {
            a = 0;
        }

        return a;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTitle, mPost, mLat, mLong;
        ImageView mImage; //inisialisasi variabel
        Button btn;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.tvDisplay);
            mPost = itemView.findViewById(R.id.tvPosition);
            mLat = itemView.findViewById(R.id.tvLat);
            mLong = itemView.findViewById(R.id.tvLong);
//            mImage = itemView.findViewById(R.id.media_image);
//            btn = itemView.findViewById(R.id.action_button_1);
//
//            btn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    String portal_content = mContent.getText().toString();
////                    Toast.makeText(c, ids, Toast.LENGTH_SHORT).show();
//                    Intent i = new Intent(c, PortalDetailActivity.class);
//                    Bundle b = new Bundle();
//
//                    Drawable drawable=mImage.getDrawable();
//                    Bitmap bitmap= ((BitmapDrawable)drawable).getBitmap();
//                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
//                    byte[] images = baos.toByteArray();
//
//                    b.putString("portal_content",portal_content);
//                    b.putByteArray("portal_image",images);
//                    b.putString("portal_title",mTitle.getText().toString());
//                    i.putExtras(b);
//                    c.startActivity(i);
//                }
//            });
        }
    }
}