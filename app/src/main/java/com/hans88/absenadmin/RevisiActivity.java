package com.hans88.absenadmin;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.hans88.absenadmin.adapters.SpinAdapter;
import com.hans88.absenadmin.models.mData.GDatas;
import com.hans88.absenadmin.models.mSave.GSave;
import com.hans88.absenadmin.models.mSpinner.GSpinner;

import java.io.File;
import java.io.FileNotFoundException;

public class RevisiActivity extends AppCompatActivity implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    Spinner spBar;
    private GSpinner lists;

    final String TAG = "GPS";
    BE.PD pd;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    static final int PICK_IMAGE = 2;

    GoogleApiClient gac;
    LocationRequest locationRequest;
    TextView tvLat, tvLong, tvLat1, tvLong1, tvLat2, tvLong2, tvLat3, tvLong3, tvLat4, tvLong4, tvAdd;
    Button btn1, btn2, btn3, btn4;
    ImageView ivP;

    SharedPreferences sp;
    SharedPreferences.Editor ed;
    Context c;

    String v;

    Bitmap bitmap;
    String pathAvatar = "";
//    RequestOptions requestOptions = new RequestOptions();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_revisi);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        c = this;
        pd = new BE.PD(c);
//        requestOptions
//                .centerCrop()
//                .placeholder(R.drawable.)
//                .error(R.drawable.)
//                .diskCacheStrategy(DiskCacheStrategy.ALL);
        sp = getApplicationContext().getSharedPreferences("sp", MODE_PRIVATE);
        ed = sp.edit();
//        ed.putString("1","0");
//        ed.putString("2","0");
//        ed.putString("3","0");
//        ed.putString("4","0");

        spBar = (Spinner) findViewById(R.id.spBar);
        tvLat = findViewById(R.id.tvLat);
        tvLong = findViewById(R.id.tvLong);
        tvLat1 = findViewById(R.id.tvLat1);
        tvLong1 = findViewById(R.id.tvLong1);
        tvLat2 = findViewById(R.id.tvLat2);
        tvLong2 = findViewById(R.id.tvLong2);
        tvLat3 = findViewById(R.id.tvLat3);
        tvLong3 = findViewById(R.id.tvLong3);
        tvLat4 = findViewById(R.id.tvLat4);
        tvLong4 = findViewById(R.id.tvLong4);
        tvAdd = findViewById(R.id.tvAdd);
        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askForPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PICK_IMAGE);
            }
        });

        btn1 = findViewById(R.id.btn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                tvLat1.setTextColor(Color.RED);
//                tvLong1.setTextColor(Color.RED);
//                ed.putString("1","1");
//                ed.putString("status","1");
//                ed.apply();
                tvLat1.setText(getLat());
                tvLong1.setText(getLong());
            }
        });
        btn2 = findViewById(R.id.btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                tvLat2.setTextColor(Color.RED);
//                tvLong2.setTextColor(Color.RED);
//                ed.putString("2","2");
//                ed.putString("status","2");
//                ed.apply();
                tvLat2.setText(getLat());
                tvLong2.setText(getLong());
            }
        });
        btn3 = findViewById(R.id.btn3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                tvLat3.setTextColor(Color.RED);
//                tvLong3.setTextColor(Color.RED);
//                ed.putString("3","3");
//                ed.putString("status","3");
//                ed.apply();
                tvLat3.setText(getLat());
                tvLong3.setText(getLong());
            }
        });
        btn4 = findViewById(R.id.btn4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                tvLat4.setTextColor(Color.RED);
//                tvLong4.setTextColor(Color.RED);
//                ed.putString("4","4");
//                ed.putString("status","4");
//                ed.apply();
                tvLat4.setText(getLat());
                tvLong4.setText(getLong());
            }
        });
//        btnDone = findViewById(R.id.btnDone);
//        btnDone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                final AlertDialog.Builder dialog = new AlertDialog.Builder(c);
//                dialog.setTitle("Save Location?")
//                        .setMessage("Make sure you already set all right")
//                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
////                                    paramDialogInterface.dismiss();
////                                    Toast.makeText(c, "Current Setting Saved", Toast.LENGTH_SHORT).show();
////                                    onBackPressed();
////                                    initPost();
////                                if (pathAvatar != "") {
//////                                    Toast.makeText(c, "Current Setting Saved", Toast.LENGTH_SHORT).show();
//////                                    Toast.makeText(c, v+" Foto : "+pathAvatar, Toast.LENGTH_SHORT).show();
////                                }else{
////                                    Toast.makeText(c, "Silahkan pilih foto", Toast.LENGTH_SHORT).show();
////                                }
//                                pd.show();
//                                initPreSave();
//                            }
//                        })
//                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                                paramDialogInterface.dismiss();
//                            }
//                        });
//                dialog.show();
//            }
//        });
        ivP = findViewById(R.id.ivPhoto);

        isGooglePlayServicesAvailable();

        if (!isLocationEnabled())
            showAlert();

        locationRequest = new LocationRequest();
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        gac = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        String permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(c, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(RevisiActivity.this, permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(RevisiActivity.this, new String[]{permission}, PICK_IMAGE);

            } else {
                initSpinner("");
            }
        } else {
            initSpinner("");
        }
    }

    private void initSpinner(String s){
        pd.show();
        AndroidNetworking.get("http://119.235.208.235:8085/api/admin-branch?brchcode="+s)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GSpinner.class, new ParsedRequestListener<GSpinner>() {
                    @Override
                    public void onResponse(GSpinner r) {
                        pd.dismiss();
                        lists = r;
                        if (lists.getData().size() > 0) {
                            ed.putString("status", "1");
                            ed.apply();
//                        List<String> listSpinner = new ArrayList<String>();
//                        for (int i = 0; i < r.getData().size(); i++){
//                            listSpinner.add(r.getData().get(i).getDisplay());
//                        }
//                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(c,
//                                android.R.layout.simple_spinner_item, listSpinner);
//                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                        spBar.setAdapter(adapter);

                            SpinAdapter adapter = new SpinAdapter(c, lists);
                            spBar.setAdapter(adapter);
                            spBar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//                                    Toast.makeText(c, lists.getData().get(pos).getValue(), Toast.LENGTH_SHORT).show();
                                    v = lists.getData().get(pos).getValue();
                                    if (null != lists.getData().get(pos).getURLImage()) {
                                        String pathS = lists.getData().get(pos).getURLImage();
                                        Uri photoUri = Uri.fromFile(new File(pathS));
                                        Glide.with(c)
                                                .load(photoUri)
                                                .into(ivP);
                                    }else {
                                        Glide.with(c)
                                                .load(R.drawable.bg_no_image)
                                                .into(ivP);
                                    }
                                    initData(v);
                                }

                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });

                        } else {
                            BE.TShort("Sorry, No Data Available");
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        pd.dismiss();
                        BE.TShort(error.getErrorDetail());
                        Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                        Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                        Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                    }
                });
    }

    private void initClearTextview(TextView lat, TextView longt){
        lat.setText("0");
        longt.setText("0");
    }

    private void initData(String v) {

        pd.show();
        AndroidNetworking.get("http://119.235.208.235:8085/api/branch-latlong?brchcode=" + v)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GDatas.class, new ParsedRequestListener<GDatas>() {
                    @Override
                    public void onResponse(GDatas r) {
                        pd.dismiss();
                        if(r.getData().size() > 0) {
                            int post = r.getData().size();
                            if (post == 1) {
                                if (null != r.getData().get(0).getPosition()) {
                                    tvLat1.setText(r.getData().get(0).getLatitude());
                                    tvLong1.setText(r.getData().get(0).getLongitude());
                                }else{
                                    initClearTextview(tvLat1,tvLong1);
                                }
                            }
                            if (post == 2) {
                                if (null != r.getData().get(1).getPosition()) {
                                    tvLat2.setText(r.getData().get(1).getLatitude());
                                    tvLong2.setText(r.getData().get(1).getLongitude());
                                }else{
                                    initClearTextview(tvLat2,tvLong2);
                                }
                            }
                            if (post == 3) {
                                if (null != r.getData().get(2).getPosition()) {
                                    tvLat3.setText(r.getData().get(2).getLatitude());
                                    tvLong3.setText(r.getData().get(2).getLongitude());
                                }else{
                                    initClearTextview(tvLat3,tvLong3);
                                }
                            }
                            if (post == 4) {
                                if (null != r.getData().get(3).getPosition()) {
                                    tvLat4.setText(r.getData().get(3).getLatitude());
                                    tvLong4.setText(r.getData().get(3).getLongitude());
                                }else{
                                    initClearTextview(tvLat4,tvLong4);
                                }
                            }
                        }else {
                            BE.TShort("Sorry, No Data Available");
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        pd.dismiss();
                        BE.TShort(error.getErrorDetail());
                        Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                        Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                        Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                    }
                });

    }

    @Override
    protected void onStart() {
        gac.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        gac.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RevisiActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

            return;
        }
        Log.d(TAG, "onConnected");

        Location ll = LocationServices.FusedLocationApi.getLastLocation(gac);
        Log.d(TAG, "LastLocation: " + (ll == null ? "NO LastLocation" : ll.toString()));

        LocationServices.FusedLocationApi.requestLocationUpdates(gac, locationRequest, this);
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(c, "Permission was granted!", Toast.LENGTH_LONG).show();
                    try {
                        LocationServices.FusedLocationApi.requestLocationUpdates(
                                gac, locationRequest, this);
                    } catch (SecurityException e) {
                        Toast.makeText(c, "SecurityException:\n" + e.toString(),
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(c, "Permission denied!", Toast.LENGTH_LONG).show();
                }

                if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
                    switch (requestCode) {
                        //Read External Storage
                        case PICK_IMAGE:
                            openFM();
                            break;
                    }
                } else {
                    Toast.makeText(this, "Ijinkan aplikasi mengakses galeri untuk merubah foto anda", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(c, "onConnectionFailed: \n" + connectionResult.toString(),
                Toast.LENGTH_LONG).show();
        Log.d("DDD", connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            updateUI(location);
        }
    }

    private void updateUI(Location loc) {
        Log.d(TAG, "updateUI");
        tvLat.setText(Double.toString(loc.getLatitude()));
        tvLong.setText(Double.toString(loc.getLongitude()));
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager =
                (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private boolean isGooglePlayServicesAvailable() {
        final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.d(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        Log.d(TAG, "This device is supported.");
        return true;
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        ed.clear();
        ed.apply();
        super.onBackPressed();
    }

    //file manager
    private void openFM() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        );
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Pilih Foto"), 1345);
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(c, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(RevisiActivity.this, permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(RevisiActivity.this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(RevisiActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            openFM();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1345 && resultCode == Activity.RESULT_OK) {

            Uri selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            if (cursor == null)
                return;
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String path = cursor.getString(columnIndex);
            cursor.close();

            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap = null;
            }

            pathAvatar = BE.getPath(c, selectedImage);

            try {
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
//                ivPhotos.setImageBitmap(bitmap);
                Glide.with(BE.getAppContext())
//                        .setDefaultRequestOptions(requestOptions)
                        .load(bitmap)
//                        .apply(RequestOptions.circleCropTransform())
                        .into(ivP);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            Log.d("Status:", "Action Not Completed");
        }
    }

    public String getLat() {
        String lat = tvLat.getText().toString();
        return lat;
    }

    public String getLong() {
        String longt = tvLong.getText().toString();
        return longt;
    }

    public void initPreSave() {
        String sLat1 = tvLat1.getText().toString();
        String sLong1 = tvLong1.getText().toString();
        String sLat2 = tvLat2.getText().toString();
        String sLong2 = tvLong2.getText().toString();
        String sLat3 = tvLat3.getText().toString();
        String sLong3 = tvLong3.getText().toString();
        String sLat4 = tvLat4.getText().toString();
        String sLong4 = tvLong4.getText().toString();
        String status = sp.getString("status", null);
        if (status.equals("1")) {
            ed.putString("status", "2");
            ed.apply();
            initSave("1", sLat1, sLong1);
        } else if (status.equals("2")) {
            ed.putString("status", "3");
            ed.apply();
            initSave("2", sLat2, sLong2);
        } else if (status.equals("3")) {
            ed.putString("status", "4");
            ed.apply();
            initSave("3", sLat3, sLong3);
        } else if (status.equals("4")) {
            ed.putString("status", "5");
            ed.apply();
            initSave("4", sLat4, sLong4);
        }
    }

    public void initSave(final String pos, String lat, String longt) {
//        pd.show();
        AndroidNetworking.post("http://119.235.208.235:8085/api/save-latlong?brchcode=" + v + "&position=" + pos + "&latitude=" + lat + "&longitude=" + longt + "&urlimage=" + pathAvatar)
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GSave.class, new ParsedRequestListener<GSave>() {
                    @Override
                    public void onResponse(GSave r) {
//                        pd.dismiss();
//                        Toast.makeText(c, r.getStatus(), Toast.LENGTH_SHORT).show();
//                        onBackPressed();
                        if (sp.getString("status", null).equals("5")) {
                            pd.dismiss();
                            Toast.makeText(c, r.getStatus(), Toast.LENGTH_SHORT).show();
                            ed.clear();
                            ed.apply();
                            initSpinner("");
                        } else {
                            initPreSave();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        pd.dismiss();
                        BE.TShort("Ulangi! Gagal menyimpan posisi " + pos);
                        BE.TShort(error.getErrorDetail());
                    }
                });
    }

    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchIem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) searchIem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public boolean onQueryTextSubmit(String query) {
                initSpinner(query);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (TextUtils.isEmpty(s)){
                    initSpinner("");
                }
                return false;
            }
        });
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            if (pathAvatar != "") {
                pd.show();
                initPreSave();
            } else {
                Toast.makeText(c, "Silahkan pilih foto", Toast.LENGTH_SHORT).show();
            }
        }

        return true;
    }
}