package com.hans88.absenadmin.models.mData;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@SerializedName("Position")
	private String position;

	@SerializedName("Value")
	private String value;

	@SerializedName("Latitude")
	private String latitude;

	@SerializedName("Longitude")
	private String longitude;

	@SerializedName("Display")
	private String display;

	public void setPosition(String position){
		this.position = position;
	}

	public String getPosition(){
		return position;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	public void setDisplay(String display){
		this.display = display;
	}

	public String getDisplay(){
		return display;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"position = '" + position + '\'' + 
			",value = '" + value + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",display = '" + display + '\'' + 
			"}";
		}
}