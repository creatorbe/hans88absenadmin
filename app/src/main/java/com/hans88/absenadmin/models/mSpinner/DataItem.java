package com.hans88.absenadmin.models.mSpinner;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@SerializedName("URLImage")
	private String uRLImage;

	@SerializedName("Value")
	private String value;

	@SerializedName("Image")
	private Object image;

	@SerializedName("Display")
	private String display;

	public void setURLImage(String uRLImage){
		this.uRLImage = uRLImage;
	}

	public String getURLImage(){
		return uRLImage;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	public void setImage(Object image){
		this.image = image;
	}

	public Object getImage(){
		return image;
	}

	public void setDisplay(String display){
		this.display = display;
	}

	public String getDisplay(){
		return display;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"uRLImage = '" + uRLImage + '\'' + 
			",value = '" + value + '\'' + 
			",image = '" + image + '\'' + 
			",display = '" + display + '\'' + 
			"}";
		}
}