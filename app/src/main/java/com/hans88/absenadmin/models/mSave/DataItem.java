package com.hans88.absenadmin.models.mSave;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@SerializedName("")
	private String jsonMember;

	public void setJsonMember(String jsonMember){
		this.jsonMember = jsonMember;
	}

	public String getJsonMember(){
		return jsonMember;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			" = '" + jsonMember + '\'' + 
			"}";
		}
}